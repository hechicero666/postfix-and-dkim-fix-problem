#!/usr/local/cwp/php71/bin/php -q
<?php

// CentOS WebPanel AutoResponder script v2

function remove_empty_line($replace){
    $replace = preg_replace('/\n/', '', $replace);
    return $replace;
}

# Settings
$postfix_password = shell_exec("grep '^password' /etc/postfix/mysql-virtual_vacation.cf| awk {'print $3'}");
$postfix_password = remove_empty_line($postfix_password);

$mysql_conn_postfix = mysqli_connect("localhost", "postfix", $postfix_password, "postfix");
$mysql_postfix = mysqli_select_db($mysql_conn_postfix, "postfix") or die("Could not connect to postfix database please check configuration!");


$recepient_split = preg_split ("/@/", $argv[2]);
$recepient_split2 = preg_split ("/#/", $recepient_split[0]);
$recepient = $recepient_split2[0]."@".$recepient_split2[1];


$ck1=mysqli_query($mysql_conn_postfix,"SHOW COLUMNS FROM vacation LIKE 'start_date' ");
if ($ck1->num_rows==0){
    $query="ALTER TABLE vacation ADD COLUMN start_date datetime NOT NULL";
    $sql = $mysql_conn_postfix->query($query);
}
$ck2=mysqli_query($mysql_conn_postfix,"SHOW COLUMNS FROM vacation LIKE 'end_date' ");
if ($ck2->num_rows==0){
    $query="ALTER TABLE vacation ADD COLUMN end_date datetime NOT NULL";
    $sql = $mysql_conn_postfix->query($query);
}
$ck2=mysqli_query($mysql_conn_postfix,"SHOW COLUMNS FROM vacation LIKE 'send_date_range' ");
if ($ck2->num_rows==0){
    $query="ALTER TABLE vacation ADD COLUMN send_date_range int NOT NULL";
    $sql = $mysql_conn_postfix->query($query); 
}


$query="SELECT vacation.email, vacation.`subject`, vacation.body, vacation.`cache`, vacation.domain, vacation.created, vacation.active, vacation.start_date, vacation.end_date, vacation.send_date_range, IF(send_date_range = '1', IF(now() BETWEEN start_date AND end_date, 1, 0), 3) AS resultado FROM vacation WHERE active='1' AND email='{$recepient}'";
$resp=mysqli_query($mysql_conn_postfix,$query);
$row = mysqli_fetch_assoc($resp);


if($resp->num_rows>0){
    if($row['resultado']==1){
        $subject = $row['subject'];
        $message = $row['body'];
        $headers = "MIME-Version: 1.0" . "\r\n" . "Content-type: text/html; charset=iso-8859-1" . "\r\n";
        $headers .= "From: {$recepient}" . "\r\n" .
            "Reply-To: {$recepient}" . "\r\n" .
            "X-Mailer: PHP/" . phpversion();
        mail($argv[1], $subject, $message, $headers);
    }

    if($row['resultado']==3){
        $subject = $row['subject'];
        $message = $row['body'];
        $headers = "MIME-Version: 1.0" . "\r\n" . "Content-type: text/html; charset=iso-8859-1" . "\r\n";
        $headers .= "From: {$recepient}" . "\r\n" .
            "Reply-To: {$recepient}" . "\r\n" .
            "X-Mailer: PHP/" . phpversion();
        mail($argv[1], $subject, $message, $headers);
    }
}
?>
